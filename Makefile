##
## EPITECH PROJECT, 2018
## PSU_myftp_2017
## File description:
## MAKEFILE
##

NAME 	=	server

CC 	=	gcc

RM	=	rm -f

INCLUDES	= ./inc

CFLAGS 	=	-W -Wall -Wextra -Werror
CFLAGS	+=	-I $(INCLUDES)

SRC 	=	src/main.c			\
		src/exec_ftp.c			\
		src/exec_part.c			\
		src/exec_partone.c		\
		src/exec_parttwo.c		\
		src/ftp_manager.c		\
		src/epur_str.c			\
		src/exec_pasv.c			\
		src/utils_check.c		\
		src/utils_more.c

OBJ 	= 	$(SRC:.c=.o)

all: 		$(NAME)

$(NAME): 	$(OBJ)
		@$(CC) $(OBJ) -o $(NAME) $(CFLAGS)
		@printf "[\033[0;35mcompilation\033[0m] % 32s\n" $(NAME) | tr ' ' '.'

clean:
	@$(RM) $(OBJ)
	@printf "[\033[0;31msuppression des .o\033[0m] % 30s\n" $(OBJ) | tr ' ' '.'

fclean: clean
	@$(RM) $(NAME)
	@printf "[\033[0;33msuppression du binaire\033[0m] % 30s\n" $(NAME) | tr ' ' '.'

re: 	fclean all

.PHONY: all clean fclean re
