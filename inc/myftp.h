/*
** EPITECH PROJECT, 2018
** PSU_myftp_2017
** File description:
** Header file
*/

#ifndef MYFTP_H
# define MYFTP_H

# include <sys/types.h>
# include <sys/socket.h>
# include <netdb.h>
# include <stdlib.h>
# include <stdio.h>
# include <netinet/in.h>
# include <arpa/inet.h>
# include <string.h>
# include <unistd.h>
# include <stdbool.h>

# define PART1 "on which the server socket listens\n"
# define HELP1 "\tport  is the port number " PART1
# define PART2 "\tpath  is the path to the home directory"
# define HELP2 PART2 " for the Anonymous user\n"
# define ERR_530 "530 Please login with USER and PASS.\r\n"
# define ERR_500 "500 Bad command.\r\n"
# define READ_SIZE 4097
# define READ_S	4096
# define DISP_PORT ",%d,%d).\r\n"
# define CR '\r'
# define NL '\n'
# define END '\0'
# define average(x, y) (x - y)

typedef enum	s_connect {
	VOID,
	PENDING,
	FIND,
	CONNECTED
}		t_connect;

typedef struct	s_ftp_cmd {
	char	*cmd_name;
	void	(*cmd)(char *buff, int *connected);
	int	need_log;
}		t_ftp_cmd;

int	new_fd;
int	s_fd;
int	c_fd;
int	passive;

void	exec_user(char *buff, int *);
void	exec_pass(char *buff, int *);
void	exec_cwd(char *buff, int *);
void	exec_cdup(char *buff, int *);
void	exec_quit(char *buff, int *);
void	exec_dele(char *buff, int *);
void	exec_pwd(char *buff, int *);
void	exec_pasv(char *buff, int *);
void	exec_port(char *buff, int *);
void	exec_help(char *buff, int *);
void	exec_noop(char *buff, int *);
void	exec_retr(char *buff, int *);
void	exec_stor(char *buff, int *);
void	exec_list(char *buff, int *);
int	ftp_manager(int fd, char *path);
int	exec_myftp(int port, char *path);
char	**wordtab(char *str, char c);
char	*epur_str(char *str);
void	exec_newfd(int port);
int	check_subfd(char *path);
int	check_fd(char *path);
char	*delete_cr(char *);
int	verif_connect(int *);
int	can_tok(char *buff);
char	*delete_cr(char *buff);
void	get_something(char *, int *);
void	disp_list(char *cmd);
void	disp_it(int *);
int	verif_co(int *connected);
void	start_ftp(char *path, FILE **subfd);
void	set_passiv(char *buff, int *connected);

#endif /* MYFTP_H */
