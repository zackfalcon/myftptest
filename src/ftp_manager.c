/*
** EPITECH PROJECT, 2018
** PSU_myftp_2017
** File description:
** Protocol gesture manager
*/

#include "myftp.h"

static t_ftp_cmd	cmd_tab[] =
{
	{"USER", &exec_user, VOID},
	{"PASS", &exec_pass, VOID},
	{"CWD", &exec_cwd, CONNECTED},
	{"CDUP", &exec_cdup, CONNECTED},
	{"QUIT", &exec_quit, VOID},
	{"DELE", &exec_dele, CONNECTED},
	{"PWD", &exec_pwd, CONNECTED},
	{"PASV", &exec_pasv, CONNECTED},
	{"PORT", &exec_port, CONNECTED},
	{"HELP", &exec_help, VOID},
	{"NOOP", &exec_noop, VOID},
	{"RETR", &exec_retr, CONNECTED},
	{"STOR", &exec_stor, CONNECTED},
	{"LIST", &exec_list, CONNECTED},
	{NULL, NULL, VOID}
};

void	init_str(char **cmd, char **name, char **buff)
{
	*buff = delete_cr(*buff);
	*cmd = strdup(*buff);
	if (can_tok(*buff) == 1)
		*name = strtok(*buff, " ");
	else
		*name = strdup(*buff);
}

int	exec_cmd(char *buff, int *connected)
{
	int	worked = 0;
	char	*cmd;
	char	*name;

	init_str(&cmd, &name, &buff);
	for (int i = 0 ; cmd_tab[i].cmd_name ; i++) {
		if (name != NULL && strcmp(name, cmd_tab[i].cmd_name) == 0) {
			if (cmd_tab[i].need_log == CONNECTED &&
			*connected != CONNECTED)
				return (dprintf(c_fd, ERR_530), 1);
			else
				cmd_tab[i].cmd(cmd, connected);
			worked = 1;
		}
	}
	if (worked == 0 && *connected == CONNECTED)
		return (dprintf(c_fd, ERR_500), 1);
	if (worked == 0 && strlen(cmd) >= 1)
		return (dprintf(c_fd, ERR_530), 1);
	return (1);
}

int	ftp_manager(int c_fd, char *path)
{
	char	*buff = NULL;
	int	connected = VOID;
	FILE	*subfd;
	size_t	size;

	start_ftp(path, &subfd);
	while (42) {
		if (getline(&buff, &size, subfd) == -1) {
			if (close(c_fd) == -1)
				return (-1);
			return (-1);
		}
		exec_cmd(buff, &connected);
		if ((strcmp(buff, "QUIT")) == 0)
			break;
	}
	if (close(c_fd) == -1)
		return (-1);
	return (0);
}
