/*
** EPITECH PROJECT, 2018
** PSU_myftp_2017
** File description:
** Exec file
*/

#include "myftp.h"

static int	set_socket(void)
{
	struct protoent	*pe = getprotobyname("TCP");

	if (pe == NULL)
		return (-1);
	s_fd = socket(AF_INET, SOCK_STREAM, pe->p_proto);
	return (s_fd);
}

static int	bind_listen(int port)
{
	struct sockaddr_in	s_n;

	s_n.sin_family = AF_INET;
	s_n.sin_port = htons(port);
	s_n.sin_addr.s_addr = INADDR_ANY;
	if (bind(s_fd, (const struct sockaddr *)&s_n, sizeof(s_n)) == -1) {
		if (close(s_fd) == -1)
			return (-1);
		return (-1);
	}
	if (listen(s_fd, 42) == -1) {
		if (close(s_fd) == -1)
			return (-1);
		return (-1);
	}
	return (1);
}

static int	connect_accept(char *path)
{
	struct sockaddr_in	s_in;
	socklen_t	s_size = sizeof(s_in);

	while (42) {
		c_fd = accept(s_fd, (struct sockaddr *)&s_in, &s_size);
		if ((check_fd(path)) == -1)
			return (-1);
	}
	return (1);
}

int	exec_myftp(int port, char *path)
{
	s_fd = set_socket();

	if (s_fd == -1)
		return (-1);
	if (bind_listen(port) == -1)
		return (-1);
	if (connect_accept(path) == -1)
		return (-1);
	return (1);
}
