/*
** EPITECH PROJECT, 2018
** PSU_myftp_2017
** File description:
** exec_user
*/

#include "myftp.h"

static void	disp_ip(char *ip)
{
	for (int i = 0 ; ip[i] ; i++)
		if (ip[i] == '.')
			dprintf(c_fd, ",");
		else
			dprintf(c_fd, "%c", ip[i]);
}

static void	calc_port(int *port1, int *port2, int realp)
{
	*port1 = (realp / 256);
	*port2 = (realp % 256);
}

void	exec_pasv(char *buff, int *connected)
{
	struct sockaddr_in	s_in;
	socklen_t	s_size = sizeof(s_in);
	char	*ip_address;
	int	port1;
	int	port2;

	set_passiv(buff, connected);
	if (new_fd != -1) {
		getsockname(new_fd, (struct sockaddr*)&s_in, &s_size);
		ip_address = inet_ntoa(s_in.sin_addr);
		calc_port(&port1, &port2, ntohs(s_in.sin_port));
		dprintf(c_fd, "227 Entering Passive Mode (");
		disp_ip(ip_address);
		dprintf(c_fd, DISP_PORT, port1, port2);
	} else {
		dprintf(c_fd, "500 Failed to entering passive mode.\r\n");
	}
}

void	exec_port(char *buff, int *connected)
{
	(void) buff;
	(void) connected;
}

void	exec_list(char *buff, int *connected)
{
	char	cmd[50];
	int	subfd = dup(1);

	strcpy(cmd, "ls -l");
	if (passive == 1) {
		get_something(buff, connected);
		disp_list(cmd);
		dup2(subfd, 1);
		close(new_fd);
	} else {
		dprintf(c_fd, "450 Use PORT or PASV first\r\n");
	}
}
