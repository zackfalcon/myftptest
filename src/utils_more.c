/*
** EPITECH PROJECT, 2018
** PSU_myftp_2017
** File description:
** exec disp it
*/

#include "myftp.h"

void	disp_it(int *connected)
{
	*connected = FIND;
	dprintf(c_fd, "331 Please specify the password.\r\n");
}

int	verif_co(int *connected)
{
	if (*connected == CONNECTED) {
		dprintf(c_fd, "530 Can't change from get user.\r\n");
		return (1);
	}
	return (-1);
}

void	start_ftp(char *path, FILE **subfd)
{
	dprintf(c_fd, "220 (vsFTPd 3.0.0)\r\n");
	*subfd = fdopen(dup(c_fd), "r");
	chdir(path);
}

void	set_passiv(char *buff, int *connected)
{
	(void) buff;
	(void) connected;
	exec_newfd(0);
	passive = 1;
}
