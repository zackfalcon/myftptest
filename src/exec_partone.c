/*
** EPITECH PROJECT, 2018
** PSU_myftp_2017
** File description:
** exec part one list
*/

#include "myftp.h"

void	exec_user(char *buff, int *connected)
{
	char	*arg;

	buff = epur_str(buff);
	if (verif_co(connected) == 1)
		return ;
	if (strcmp(buff, "USER") != 0) {
		arg = strtok(buff, " ");
		arg = strtok(NULL, " ");
		if (strcasecmp(arg, "Anonymous") == 0) {
			disp_it(connected);
			return ;
		}
	} else {
		dprintf(c_fd, "530 Permission denied.\r\n");
		return ;
	}
	if (arg != NULL && strlen(arg) > 2)
		*connected = PENDING;
	dprintf(c_fd, "331 Please specify the password.\r\n");
}

void	exec_pass(char *buff, int *connected)
{
	(void) buff;
	if (verif_connect(connected) == 1)
		return ;
	if (*connected == PENDING) {
		*connected = VOID;
		dprintf(c_fd, "530 Login incorrect.\r\n");
		return ;
	}
	if (*connected == VOID) {
		dprintf(c_fd, "503 Login with USER first\r\n");
		return ;
	} else {
		dprintf(c_fd, "530 Login incorrect.\r\n");
	}
}

void	exec_cwd(char *buff, int *connected)
{
	(void) connected;
	char	*arg;

	buff = epur_str(buff);
	if (strcmp(buff, "CWD") != 0) {
		arg = strtok(buff, " ");
		arg = strtok(NULL, " ");
		if (chdir(arg) == 0)
			dprintf(c_fd, "250 Workded !\r\n");
		else
			dprintf(c_fd, "550 Bad dir.\r\n");
	} else {
		dprintf(c_fd, "550 Failed to change directory.\r\n");
	}
}

void	exec_cdup(char *buff, int *connected)
{
	(void) buff;
	(void) connected;
	if (chdir("..") == 0)
		dprintf(c_fd, "200 Worked !\r\n");
	else
		dprintf(c_fd, "550 Cannot change dir\r\n");
}

void	exec_quit(char *buff, int *connected)
{
	(void) buff;
	(void) connected;
	dprintf(c_fd, "221 Goodbye.\r\n");
}
