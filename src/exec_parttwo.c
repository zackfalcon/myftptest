/*
** EPITECH PROJECT, 2018
** PSU_myftp_2017
** File description:
** exec_file list
*/

#include "myftp.h"

void	exec_dele(char *buff, int *connected)
{
	(void) connected;
	char	*arg;

	buff = epur_str(buff);
	if (strcmp(buff, "DELE") != 0) {
		arg = strtok(buff, " ");
		arg = strtok(NULL, " ");
		if (unlink(arg) == 0)
			dprintf(c_fd, "250 Workded !\r\n");
		else
			dprintf(c_fd, "450 Bad file.\r\n");
	} else {
		dprintf(c_fd, "550 Permission denied.\r\n");
	}
}

void	exec_pwd(char *buff, int *connected)
{
	(void) buff;
	(void) connected;
	char	*path = NULL;

	path = getcwd(path, 4096);
	dprintf(c_fd, "257 %s\r\n", path);
}

void	exec_help(char *buff, int *connected)
{
	(void) buff;
	(void) connected;
	dprintf(c_fd, "214 The Following commands are recognized.\r\n");
	dprintf(c_fd, "USER PASS CWD CDUP DELE PWD PASV ");
	dprintf(c_fd, "PORT HELP NOOP RETR STOR LIST\r\n");
	dprintf(c_fd, "214 Help OK.\r\n");
}

void	exec_noop(char *buff, int *connected)
{
	(void) buff;
	(void) connected;
	dprintf(c_fd, "200 NOOP ok.\r\n");
}
