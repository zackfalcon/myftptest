/*
** EPITECH PROJECT, 2018
** PSU_myftp_2017
** File description:
** Utils check
*/

#include "myftp.h"

int	check_subfd(char *path)
{
	if (fork() == 0)
		ftp_manager(c_fd, path);
	else
		if (close(c_fd) == -1)
			return (-1);
	return (1);
}

int	check_fd(char *path)
{
	if (c_fd == -1) {
		if (close(s_fd) == -1)
			return (-1);
		return (-1);
	} else {
		if ((check_subfd(path)) == -1)
			return (-1);
	}
	return (1);
}

void	exec_retr(char *buff, int *connected)
{
	(void) buff;
	(void) connected;
}

void	exec_stor(char *buff, int *connected)
{
	(void) buff;
	(void) connected;
}

int	verif_connect(int *connected)
{
	if (*connected == CONNECTED) {
		dprintf(c_fd, "230 Already logged in\r\n");
		return (1);
	}
	if (*connected == FIND) {
		*connected = CONNECTED;
		dprintf(c_fd, "230 Connected\r\n");
		return (1);
	}
	return (-1);
}
