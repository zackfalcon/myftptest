/*
** EPITECH PROJECT, 2018
** PSU_myftp_2017
** File description:
** Main file
*/

#include "myftp.h"

void	disp_help(void)
{
	printf("USAGE:  ./server port path\n");
	printf(HELP1);
	printf(HELP2);
}

int	main(int argc, char **argv)
{
	if (argc != 3) {
		disp_help();
		return (84);
	} else {
		if (exec_myftp(atoi(argv[1]), argv[2]) == -1)
			return (84);
	}
	return (0);
}
