/*
** EPITECH PROJECT, 2018
** PSU_myftp_2017
** File description:
** EXEC FTP PORTOCOL
*/

#include "myftp.h"

static int	current_ip(int fd)
{
	struct sockaddr_in	s_in;
	socklen_t	s_size = sizeof(s_in);
	int	current_addr;

	if (fd == -1)
		return (htons(INADDR_ANY));
	getsockname(fd, (struct sockaddr *)&s_in, &s_size);
	current_addr = s_in.sin_addr.s_addr;
	return (current_addr);
}

static int	bind_newlisten(int port)
{
	struct protoent	*pe = getprotobyname("TCP");
	struct sockaddr_in	in;

	if (pe == NULL)
		return (-1);
	new_fd = socket(AF_INET, SOCK_STREAM, pe->p_proto);
	in.sin_family = AF_INET;
	in.sin_port = htons(port);
	in.sin_addr.s_addr = current_ip(c_fd);
	if (bind(new_fd, (const struct sockaddr *)&in, sizeof(in)) == -1) {
		if (close(s_fd) == -1)
			return (-1);
		if (close(c_fd) == -1)
			return (-1);
		if (close(new_fd) == -1)
			return (-1);
		return (-1);
	}
	return (1);
}

static int	new_listen(void)
{
	if (listen(new_fd, 42) == -1) {
		if (close(s_fd) == -1)
			return (-1);
		if (close(c_fd) == -1)
			return (-1);
		if (close(new_fd) == -1)
			return (-1);
		return (-1);
	}
	return (1);
}

void	exec_newfd(int port)
{
	bind_newlisten(port);
	new_listen();
}
